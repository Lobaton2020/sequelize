const express = require("express")
const app = express()
const sequelize = require("./database/db")
const User = require("./database/models/User")
//Settings
const port = process.env.PORT || 3000
//Routes
app.get('/', async (req, res) => {
    try {
        const user = await User.create({
            name: "Los pepos 2020",
            birthday: new Date(1983, 4, 5)
        })
        res.send({
            type: "success",
            data: user
        })
    } catch (err) {
        res.send({
            type: "error",
            data: []
        })
        console.log(err)
    }
})
app.get('/users', async (req, res) => {
    try {
        const users = await User.findAll()
        res.send({
            type: "success",
            data: users
        })
    } catch (err) {
        res.send({
            type: "error",
            data: []
        })
        console.log(err)
    }
})
//Initializer app
app.listen(port, async () => {
    console.log(`APP RUNNING IN PORT http://localhost:${port}`)
    try {
        await sequelize.sync({ force: false })
        console.log("CONECTADO CORRECTAMENTE")
    } catch (err) {
        console.log(err)
    }
})