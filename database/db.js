const { Sequelize } = require("sequelize")
const { hostname, name, username, password, port, dialect } = require("../config").database

const sequelize = new Sequelize(name, username, password, {
    host: hostname,
    dialect: dialect
})
module.exports = sequelize